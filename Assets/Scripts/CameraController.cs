﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player; // This is for use to attach our player object
    public float smooth = 0.3f; // This will control how fast the camera will move towards the player
    public float cameraDistance; // This will control the distance away from the player

    public float height; // This will contol how high the camera is to the player
   
    private Vector3 velocity = Vector3.zero; // This will ensure the cameras velocity starts at 0 

    private void Update()
    {
        Vector3 pos = new Vector3(); // this will create the new x,y,z postion of the camera on the player
        pos.x = player.position.x; // this is the players position on the x plane
        pos.z = player.position.z - cameraDistance; // This will determine the distance from the player as the player moves
        pos.y = player.position.y + height; // this will determine how the camera moves up and down as the player does
        transform.position = Vector3.SmoothDamp(transform.position, pos, ref velocity, smooth); // this will begin to move the camera and follow the player
    }
}
