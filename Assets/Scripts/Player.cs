﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField, Tooltip("The max speed of the player")]
    private float speed = 4f; // this is the max speed our player will move

    private Animator animator; // this will allow us to attach the animator that we created onto the script

    public new GameObject camera; // this will allow us to attach the camera so it knows who to follow

    private void Awake()
    {
        animator = GetComponent<Animator>();    // this will automatically attach the animator for us before the game starts    
    }

    // Update is called once per frame
    private void Update()
    {
        // Player movement
        Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical")); // this will help move the player when we use the keyboard
        input = Vector3.ClampMagnitude(input, 1f); // this will ensure that we dont move to fast when we move diagonally. If this werent here, we would move faster diagonally than we would forward and backwards
        input *= speed; // this will take our speed and multiply it by more speed
        animator.SetFloat("Horizontal", input.x); // this will apply the speed at which were moving and set the animations for us
        animator.SetFloat("Vertical", input.z); // same as above

        // Player facing mouse
        Plane playerPlane = new Plane(Vector3.up, transform.position);
        Ray ray = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition); // this will create a ray cast on the game that our mouse is pointing at
        float hitDist = 0.0f;

        if(playerPlane.Raycast(ray,out hitDist)) // creates a plane that is centered on the object on the X and Z directions
        {
            Vector3 targetPoint = ray.GetPoint(hitDist); // 
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position); // this will roate the player towards the mouse
            targetRotation.x = 0;
            targetRotation.z = 0;
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 7f * Time.deltaTime); // move to the positon that the mouse is point at
        }
    }
}
